// NUMBER OF CHARACTERS TO WRITE
var stringLength = Math.floor(Math.random() * (1000 - 0) + 0);
// TODO : Add a preliminary step with punctuation, letters, numbers, spaces ? or new "function" like STOP ?
var possibleValues = ['STOP',' ','a','z','e','r','t','y','u','i','o','p','q','s','d','f','g','h','j','k','l','m','w','x','c','v','b','n'];
var finalString = '';



$(document).ready(function() {
	$("#console").html($("#console").html() + 'Welcome to randomLetters. Please enter a prompt :<br><br>');

	$('.prompt').keypress(function(e) {
		// 13 -> Enter
		if(e.which == 13)
		{
			$("#console").html($("#console").html() + '<p class="prompt-archive">' + escapeHtml($('.prompt').val()) + '</p>'); // Add the line we just wrote
			$('.prompt').val(''); // Remove the input text

			

			console.log('Starting the loop - stringLength = ' + stringLength);


			//---------------------------------------------------------------------------------------------
			// Test using ANU QRNG Quantum random numbers
			// 
			$.ajax({
				url: "https://qrng.anu.edu.au/API/jsonI.php",
				data: 
				{
					length: stringLength,
					type: 'uint8',
					size: 6
				}
			}).done(function( QRNGCryptoArray ) 
			{
				console.log( QRNGCryptoArray );

				for(var i = 0; i <= QRNGCryptoArray.data.length; i++)
				{
					if(possibleValues[QRNGCryptoArray.data[i]] == 'STOP')
					{
						console.log('+ STOP (0)');
						break;
					}
					else if(QRNGCryptoArray.data[i] < possibleValues.length)
					{
						finalString = finalString + possibleValues[QRNGCryptoArray.data[i]];
						console.log('+ ' + possibleValues[QRNGCryptoArray.data[i]] + ' (' + QRNGCryptoArray.data[i] + ')');
					} else {
						console.log('- OUT OF RANGE (' + QRNGCryptoArray.data[i] + ')');
					}
				}

				// $("#console").html($("#console").html() + '<p>' + finalString + '<br>' + loopTheString(finalString) + '</p>');
				$("#console").html($("#console").html() + finalString + '<br>');
				for (var i = 0; i < 108 ; i++)
				{
					$("#console").html($("#console").html() + loopTheString(finalString) + '<br>');
				}
				$("#console").html($("#console").html() + '<br/>'); // Final line jump
				$('#console').animate( { scrollTop: $('#console')[0].scrollHeight }, 2000 ); // Scroll the console
			});

			finalString = ''; // Reset the string
		}
		
	});

	//---------------------------------------------------------------------------------------------
	// Keyboard stuff
	// 

	$("#keyboard").on("click", function(e) {
		$.ajax({
			'type' : 'POST',
			'dataType' : 'script',
			//'data' : $.parseJSON( '{"action" : "home_filter", "term_id" : "' + $(this).attr('value') + '"}' ),
			'url' : 'keyboard.php',
			'success' : function(data)
			{
				console.log('KEYBOARD \\o/');
				$('#prompt-keyboard').html('X' + $('#prompt-keyboard').html());
			},
			'error' : function(request,error)
			{
				console.log('////////////////////////////////////////////////   ERROR   //////////////////////////////////');
				console.log(error);
				console.log(request);
			},
		});

		
	});

});




// HTML escaping, just in case
var entityMap = {
	'&': '&amp;',
	'<': '&lt;',
	'>': '&gt;',
	'"': '&quot;',
	"'": '&#39;',
	'/': '&#x2F;',
	'`': '&#x60;',
	'=': '&#x3D;'
};

function escapeHtml (string) {
	return String(string).replace(/[&<>"'`=\/]/g, function (s) {
		return entityMap[s];
	});
}

// Attempt to boost entropy with loops
function loopTheString (string)
{
	// We generate one uint per character still in the string
	var randomCryptoArray = new Uint8Array(string.length); // Uint8 are between 0 - 255, and we generate one value per character we need
	window.crypto.getRandomValues(randomCryptoArray);
	console.log(randomCryptoArray);

	// Loop on each of the string characters
	for(var i = 0 ; i < string.length ; i++)
	{
		// console.log('i = ' + i + ' string.charAt(i) = ' + string.charAt(i) );

		// Character replacement and deletion loop
		if(possibleValues[randomCryptoArray[i]] == 'STOP')
		{
			console.log('X STOP (0), DELETING CHARACTER');
			// Here we delete the character
			string =  string.substr(0, i) + string.substr(i, string.length - 1);
		}
		else if(randomCryptoArray[i] < possibleValues.length)
		{
			string =  string.substr(0, i) + possibleValues[randomCryptoArray[i]] + string.substr(i, string.length - 1);
			console.log('+ ' + string.charAt(i) + ' -> ' + possibleValues[randomCryptoArray[i]] + ' (' + randomCryptoArray[i] + ')');
		} else {
			console.log('- OUT OF RANGE (' + randomCryptoArray[i] + ')');
		}

		// Character change in relation to the possibleValues array based on Math.random()
		var num = Math.floor(Math.random() * (100 - 0) + 0);
		// 5% chance to go down
		if(num <= 5)
		{
			var index = possibleValues.indexOf(string.charAt(i));
			console.log("<<< moving " + string.charAt(i) + " ( " + index + " ) to " + possibleValues[index-1]);

			if(possibleValues[index-1] == 'STOP')
			{
				console.log('X STOP (0), DELETING CHARACTER');
				// Here we delete the character
				string =  string.substr(0, i) + string.substr(i, string.length - 1);
			} else {
				string = string.substr(0, i) + possibleValues[index-1] + string.substr(i, string.length - 1);
			}

			
		} 
		else if (num >= 95) // 5% chance to go up
		{
			var index = possibleValues.indexOf(string.charAt(i));
			console.log(">>> moving " + string.charAt(i) + " ( " + index + " ) to " + possibleValues[index+1]);

			if(index+1 > possibleValues.length)
			{
				console.log('X EXEDING ARRAY, DELETING CHARACTER');
				// Here we delete the character
				string =  string.substr(0, i) + string.substr(i, string.length - 1);
			} else {
				string = string.substr(0, i) + possibleValues[index+1] + string.substr(i, string.length - 1);
			}
		}
	}

	return string;
}